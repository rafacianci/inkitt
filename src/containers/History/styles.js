import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  title: {
    alignSelf: 'center',
    fontSize: 20,
    fontWeight: '700',
    marginBottom: 20,
    marginTop: 15,
  },
  button: {
    backgroundColor: '#4285F4',
    borderRadius: 4,
    flex: 1,
    marginHorizontal: 20,
    padding: 10,
  },
  buttonText: {
    color: '#FFF',
    fontSize: 18,
    textAlign: 'center',
  },
});
