import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { getHistory } from '../../actions/history';
import parseHTML from '../../components/parseHTML';
import styles from './styles';

class History extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  componentWillMount() {
    this.getHistoryChapter(1);
  }

  getHistoryChapter(chapter) {
    this.setState({
      isLoading: true,
    });

    this.props.getHistory(chapter).finally(() => (
      this.setState({
        isLoading: false,
      })
    ));
  }

  renderButton() {
    if (this.props.data.chapter_number === 1) {
      return (
        <TouchableOpacity onPress={() => this.getHistoryChapter(2)} style={styles.button}>
          <Text style={styles.buttonText}>
            Next Chapter
          </Text>
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity onPress={() => this.getHistoryChapter(1)} style={styles.button}>
        <Text style={styles.buttonText}>
          Last Chapter
        </Text>
      </TouchableOpacity>
    );
  }

  render() {
    if (this.state.isLoading) {
      return (
        <Text>Loading...</Text>
      );
    }

    if (this.props.error) {
      return (
        <View>
          <Text>{this.props.error}</Text>
        </View>
      );
    }

    return (
      <ScrollView>
        <Text style={styles.title}>
          { this.props.data.name }
        </Text>
        { parseHTML(this.props.data.text) }
        { this.renderButton() }
      </ScrollView>
    );
  }
}


const mapStateToProps = ({ history }) => ({
  data: history.data,
  error: history.error,
});

export default connect(mapStateToProps, {
  getHistory,
})(History);

