import { get } from '../utils/http';
import {
  GET_HISTORY_SUCCESS,
  GET_HISTORY_ERROR,
} from './types';

export const getHistory = chapter => dispatch => (
  get(`stories/106766/chapters/${chapter}`).then(
    response => dispatch({
      type: GET_HISTORY_SUCCESS,
      payload: response.data,
    }),
    error => dispatch({
      type: GET_HISTORY_ERROR,
      payload: error,
    }),
  )
);
