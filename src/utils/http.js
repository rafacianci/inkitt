import axios from 'axios';

export const setDefaults = () => {
  axios.defaults.baseURL = 'http://cap_america.inkitt.de/1/';
  axios.defaults.headers.post['Content-Type'] = 'application/json';
  axios.defaults.headers.post.Accept = 'application/json';
};

export const get = url => axios.get(url);
