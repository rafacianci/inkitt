import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  i: {
    fontStyle: 'italic',
  },
  b: {
    fontWeight: '700',
  },
  em: {
    fontStyle: 'italic',
  },
  strong: {
    fontWeight: '700',
  },
  defaultText: {
    fontSize: 18,
    lineHeight: 24,
  },
  paragraph: {
    marginBottom: 30,
    paddingHorizontal: 20,
  },
});
