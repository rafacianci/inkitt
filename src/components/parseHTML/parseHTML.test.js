import React from 'react';
import renderer from 'react-test-renderer';
import ParseHTML from './ParseHTML';

test('renders without crashing', () => {
  const rendered = renderer.create(<ParseHTML html='<p>Test of parse HTML</p>' />).toJSON();
  expect(rendered).toBeTruthy();
});
