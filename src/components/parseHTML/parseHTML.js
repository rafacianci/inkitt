import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const supportedTags = ['i', 'b', 'em', 'strong'];

const getTag = (tag = 'normal', text, index, mapIndex) => (
  <Text style={styles[tag]} key={`${tag}_${index}-${mapIndex}`}>
    {text}
  </Text>
);

const styleReplace = (textArray, index) => {
  let currentTag;
  let isInside;

  return textArray.map((oldText, mapIndex) => {
    const text = oldText.replace(/<\/p[^>]*>/g, '');
    if (!text) {
      return null;
    }

    if (currentTag && currentTag !== text && (supportedTags.indexOf(text) >= 0)) {
      isInside = currentTag;
    }

    if (currentTag === text) {
      currentTag = undefined;
      isInside = false;
      return null;
    }

    if (supportedTags.indexOf(text) >= 0) {
      currentTag = text;
      return null;
    }

    if (isInside) {
      return getTag(isInside, getTag(currentTag, text, index, mapIndex), index - 1, mapIndex - 1);
    }

    return getTag(currentTag, text, index, mapIndex);
  });
};

const TextContainer = (text, index) => (
  <View key={`content_${index}`} style={styles.paragraph}>
    <Text style={styles.defaultText}>
      { text }
    </Text>
  </View>
);

const mapHTML = htmlArrayMap => (
  htmlArrayMap.map((text, index) => {
    if (text.length > 1) {
      return TextContainer(styleReplace(text, index), index);
    }

    if (!text[0]) {
      return null;
    }

    return TextContainer(text[0].replace(/<\/p[^>]*>/g, ''), index);
  })
);

const parseHTML = html => (
  mapHTML(
    html
      .replace(' class=""', '')
      .replace(/<br[^>]*>/g, '\n')
      .split(/<p[^>]*>/g)
      .map(text => text.split(/<\/?(b|i|em|strong)[^>]*>/g)),
  )
);

export default parseHTML;
