import {
  GET_HISTORY_SUCCESS,
  GET_HISTORY_ERROR,
} from '../actions/types';

const INITIAL_STATE = {
  data: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_HISTORY_SUCCESS:
      return {
        data: action.payload.response,
      };
    case GET_HISTORY_ERROR:
      console.log(action.payload);
      return {
        error: 'An unexpected error ocurred, please contact us.',
      };
    default:
      return state;
  }
};
