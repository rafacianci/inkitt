# Inkitt test #

### How do I get set up? ###

* Install NodeJS, NPM [Learn how](https://nodejs.org/en/download/)
* Install Yarn (recomended) [here](https://yarnpkg.com/en/docs/install)

#### On Ios ####
* you can follow these steps to get ready. [here](https://facebook.github.io/react-native/docs/running-on-simulator-ios.html)

#### On Windows/Linux ####
* you can follow these steps to get ready. [here](https://medium.com/@deepak.gulati/running-react-native-app-on-the-android-emulator-11bf309443eb)

### Getting start ###

Inside that directory, you can run several commands:

```
  yarn start
    Starts the development server so you can open your app in the Expo
    app on your phone.
```
```
  yarn run ios
    (Mac only, requires Xcode)
    Starts the development server and loads your app in an iOS simulator.
```
```
  yarn run android
    (Requires Android build tools)
    Starts the development server and loads your app on a connected Android
    device or emulator.
```