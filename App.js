import React, { Component } from 'react';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { StyleSheet, View } from 'react-native';
import { setDefaults } from './src/utils/http';
import reducers from './src/reducers';
import History from './src/containers/History';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 25,
  },
});

class App extends Component {
  componentWillMount() {
    setDefaults();
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <View style={styles.container}>
        <Provider store={store}>
          <History />
        </Provider>
      </View>
    );
  }
}

export default App;
